﻿using System.Reflection.Emit;
using System;
using static System.Console;

namespace CodeExp
{
    class Program
    {
        static void Main(string[] args)
        {

            GeneratingGrid();
            FarmerActions.NextAction();

            void GeneratingGrid()
            {
                char rownb = 'a';
                int x = 5;
                int columnNB = 1;
                for (int i = 0; i < x; i++)
                {
                    Write(columnNB + " ");
                    columnNB++;
                }
                WriteLine();
                for (int i = 0; i < x; i++)
                {
                    Write(rownb + " ");
                    rownb++;

                    string[] row = { Plot.GenerateRandomPlot().PlotFormatter(), Plot.GenerateRandomPlot().PlotFormatter(), Plot.GenerateRandomPlot().PlotFormatter(), Plot.GenerateRandomPlot().PlotFormatter(), Plot.GenerateRandomPlot().PlotFormatter(), };
                    foreach (var singleplot in row)
                    {
                        Write(singleplot + " ");
                    }
                    WriteLine("");
                }
            }
        }
    }
}
