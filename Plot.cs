using System;
namespace CodeExp
{
    public class Plot
    {
        public char content;
        public enum plantKind
        {
            CARROT,
            POTATO,
            BLUEBERRY,
            WATERMELON,
            EMPTY,
            DEAD
        }

        public plantKind kind;
        public int wateredAmount = 0;

        public string PlotFormatter()
        {
            string leftSide = "[", rightSide = "]";
            return leftSide + content + rightSide;
        }

        public void InitializeContent(plantKind kind)
        {
            switch (kind)
            {
                case Plot.plantKind.CARROT:
                    content = 'C';
                    break;
                case Plot.plantKind.POTATO:
                    content = 'P';
                    break;
                case Plot.plantKind.BLUEBERRY:
                    content = 'B';
                    break;
                case Plot.plantKind.WATERMELON:
                    content = 'W';
                    break;
                case Plot.plantKind.EMPTY:
                    content = ' ';
                    break;
                case Plot.plantKind.DEAD:
                    content = 'x';
                    break;
            }
        }

        public static Plot GenerateRandomPlot()
        {
            Plot plot = new Plot();
            Random rnd = new Random();
            var val = rnd.Next(6);
            switch (val)
            {
                case 0:
                    plot.kind = plantKind.CARROT;
                    break;
                case 1:
                    plot.kind = plantKind.POTATO;
                    break;
                case 2:
                    plot.kind = plantKind.BLUEBERRY;
                    break;
                case 3:
                    plot.kind = plantKind.WATERMELON;
                    break;
                case 4:
                    plot.kind = plantKind.EMPTY;
                    break;
                case 5:
                    plot.kind = plantKind.DEAD;
                    break;
                default:
                    Console.WriteLine("Generated some wrong values in GenerateRandomPlot");
                    break;
            }
            plot.InitializeContent(plot.kind);
            return plot;
        }
    }
}