# Features
every plot should have :
- If there is something planted in it or not
- whether it's alive or dead
- keeping track of how many times the plant has been watered

- Location system (grid like a chess board ?)

- seasons with 20 days each

- limited amount of actions per day (6)

- if you plant a starfruit for example, you need to water it 14 days in the month

- the d6 and everything associated with it