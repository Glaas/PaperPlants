using static System.Console;

namespace CodeExp
{
    public static class FarmerActions
    {
        public static int myAge = 24;

        public static void PlantPlot(Plot plot, Plot.plantKind kind)
        {
            plot.kind = kind;
        }
        public static void WaterPlant(Plot plot)
        {
            plot.wateredAmount++;
        }
        public static void NextAction()
        {
            WriteLine("Please enter your target");

            //TODO: Make the plot selection system
            //var input = ReadLine();
            Plot plott = new Plot();


            WriteLine("Your target is plott. What actions to you want to take ?");
            WriteLine("Press P to plant, W to water a plant");
            string input = ReadLine();
            input = input.ToUpper();
            WriteLine("Your input was " + input);

            switch (input)
            {
                case "P":
                    WriteLine("You decided to plant the plot");
                    bool invalidInput = true;
                    do
                    {
                        WriteLine("Your choices are Carrot, Potato, Blueberry and Watermelon");
                        input = ReadLine();
                        input = input.ToUpper();
                        invalidInput = false;
                        switch (input)
                        {
                            case "C": //Carrot
                                WriteLine("You chose to plant a Carrot");
                                FarmerActions.PlantPlot(plott, Plot.plantKind.CARROT);
                                break;
                            case "P": //Potato
                                WriteLine("You chose to plant a Potato");
                                FarmerActions.PlantPlot(plott, Plot.plantKind.POTATO);
                                break;
                            case "B": //Blueberry
                                WriteLine("You chose to plant a Blueberry");
                                FarmerActions.PlantPlot(plott, Plot.plantKind.BLUEBERRY);
                                break;
                            case "W": //Watermelon
                                WriteLine("You chose to plant Watermelon");
                                FarmerActions.PlantPlot(plott, Plot.plantKind.WATERMELON);
                                break;
                            default:
                                WriteLine("Choose a valid plant type, dumbass");
                                invalidInput = true;
                                break;
                        }
                    } while (invalidInput);
                    invalidInput = false;
                    break;
                case "W":
                    WriteLine("You decided to water the plant");
                    FarmerActions.WaterPlant(plott);
                    WriteLine("The amount of watering for plott is now " + plott.wateredAmount);
                    break;
                default:
                    WriteLine("wrong input dumbass, learn to type");
                    break;

            }
        }


    }
}